const express = require('express')
const app = express()
const fs = require("fs")
const path =require("path")
const port = 3000
const moment = require('moment-timezone');



app.set("view engine","ejs")
app.set("views",path.join(__dirname,"/views"))

app.use(express.static(path.join(__dirname,"public")))

app.use(express.urlencoded({ extended:true}))
app.use(express.json())

//getting the file read 
const tours = JSON.parse( fs.readFileSync(`${__dirname}/data/tours-simple.json`))

//middleware
app.use(express.json())

app.get('/', (req, res) => res.send('Hello World!'))

// app.use('/pp',(req,res)=>{
//     console.log("pp is request")
//     res.send("request get")
// })
 
// app.get('/obj',(req,res)=>{
//     res.send("<h1>Hii!!!!!!</h1>")
// })

// app.get('/:username/:id',(req,res)=>{
//     let {username,id}= req.params
//     res.send(`welcome user ${username}  and ${id}`)
//})
//routing
app.get('/home',(req,res)=>{
    res.render("home.ejs")
})
//ejs
app.get('/rolldice',(req,res)=>{
   let diceRoll =Math.floor(Math.random() * 6)+1
    res.render("rollDice",{diceRoll})
})



//handing the get requset
app.get ('/api/v1/tours',(req,res)=>{
    res.status(200).json({
        "status":"succed",
        "data" : {
            tours
        }
    })
})

//handling the post request
app.post('/api/v1/tours',(req,res)=>{
   // console.log(req.body)
   const newId = tours[tours.length - 1].id +1
   const newTours = Object.assign({id : newId},req.body) 

   tours.push(newTours)
   fs.writeFile(`${__dirname}/data/tours-simple.json`,JSON.stringify(tours),err =>{
    res.status(201).json({
        "status":"succcess",
        "data":{
            tours :newTours
        }
    })
   })

 //res.send("Done")

})

// Get data using ny "id"
app.get ('/api/v1/tours/:id',(req,res)=>{
   // console.log(req.params)
    const id = req.params.id
    console.log(id)
    const tour = tours.find(one => one.id  == id)
    console.log(tour)
    res.status(200).json({
        "status":"succed",
        "data" : {
            tour
        }

    })
})


app.get('/api/timezones', (req, res) => {
    const timeZones = [
      //  'America/New_York', // New York
        'Europe/London',    // London
        'Asia/Tokyo',       // Tokyo
        'Australia/Sydney', // Sydney
        'America/Los_Angeles',
         'Asia/Kolkata'
    ];

    const currentTimes = timeZones.map(zone => ({
        zone,
        currentTime: moment().tz(zone).format('YYYY-MM-DD HH:mm:ss')
    }));

    res.json(currentTimes);
});


app.listen(port, () => console.log(`Example app listening on port ${port}!`))


